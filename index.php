<?php

require_once('core/init.php');

function homeFunction()
{
	echo 'Hello from Home.';
}

function loginFunction()
{
	echo 'Hello from Login.';
}

$router = new \Igniter\Router;
$router->route('/','homeFunction');
$router->route('/login', 'loginFunction');
$router->route('/users', 'getAllUserInformation');
$router->route('/subjects', 'getAllSubjectsInformation');
$router->route('/subject/<:subjectid|[0-9]{6}>', 'getSubjectInformation');
$router->route('/faculty', 'getAllFacultyInformation');
$router->route('/students', 'getAllStudentInformation');
$router->route('/<:username>', 'getUserInformation');


$router->execute();