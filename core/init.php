<?php


/*
 * Include Configuration and Global Function files.
 */
require_once('core/config.php');
require_once('core/functions.php');


/*
 * Include Router and Database class files.
 */
require_once('core/includes/classes/Router.php');
require_once('core/includes/classes/Database.php');
require_once('core/includes/classes/Template.php');


/*
 * Include functional files
 */
require_once('core/includes/users.php');
require_once('core/includes/subjects.php');
