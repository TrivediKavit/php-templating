<?php

function getUserInformation($param)
{
    $param = getParameter($param,getLength($param)-1);
    $user = new StdClass;
    $subjects = new StdClass;
    $DB = DataBase::getInstance();
    $user = $DB->get('users',array('username', '=', $param));
    if($user->count() == 1)
    {
        $user = $user->first();
        echo 'ID : ' . $user->id;
        echo '<br />Username : ' . $user->username;

        $subjects = $DB->query("SELECT `subjects`.`subject_id`,`subjects`.`subject_name`,`subjects`.`subject_credits`,`subjects`.`subject_semester`
                                ,`classes`.`name` AS `class_name`
                                FROM  `subjects`,`classes`,`faculty-subjects`
                                WHERE  `subjects`.`subject_id` =  `faculty-subjects`.`subject_id` 
                                AND `faculty-subjects`.`user_id` = {$user->id}
                                AND `faculty-subjects`.`class_id` = `classes`.`id`;");

        echo '<br /><h1>Subjects Assigned</h1>';
        if($subjects->count())
        {
            foreach ($subjects->results() as $subject)
            {
                echo 'Subject ID : ' . $subject->subject_id;
                echo '<br />Subject Name : ' . $subject->subject_name;
                echo '<br />Subject Credits : ' . $subject->subject_credits;
                echo '<br />Class : ' . $subject->class_name;
                echo '<br />Semester : ' . $subject->subject_semester;
                echo '<br /><br />';
            }
        }
        else
        {
            echo 'No subjects assigned.';
        }
    }
    else
    {
        echo 'No Such User found.';
    }
} 

function getAllUserInformation()
{
    $users = new StdClass;
    $DB = DataBase::getInstance();
    $users = $DB->query('SELECT * FROM `users`');
    if($users->count())
    {
        foreach ($users->results() as $user)
        {
            echo 'ID : ' . $user->id;
            echo '<br />Username : ' . $user->username;
            echo '<br />';
        }
    }
    else
    {
        echo 'No Users found.';
    }
}

function getAllStudentInformation()
{
    $users = new StdClass;
    $DB = DataBase::getInstance();
    $users = $DB->get('users',array('userlevel', '=', '2'));
    if($users->count())
    {
        foreach ($users->results() as $user)
        {
            echo 'ID : ' . $user->id;
            echo '<br />Username : ' . $user->username;
            echo '<br />';
        }
    }
    else
    {
        echo 'No Users found.';
    }
}

function getAllFacultyInformation()
{
    $users = new StdClass;
    $DB = DataBase::getInstance();
    $users = $DB->get('users',array('userlevel', '=', '1'));
    if($users->count())
    {
        foreach ($users->results() as $user)
        {
            echo 'ID : ' . $user->id;
            echo '<br />Username : ' . $user->username;
            echo '<br />';
        }
    }
    else
    {
        echo 'No Users found.';
    }
}