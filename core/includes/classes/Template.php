<?php


class Template
{
	private $vars = array();

	public function assign($key=NULL, $value=NULL)
	{
		if(!is_array($key))
		{
			$this->vars[$key] = $value;
		}
		else
		{
			foreach ($key as $k => $v)
			{
				$this->vars[$k] = $v;
			}
		}
	}

	public function render($template_name)
	{
		$path = 'views/' . $template_name . '.html';

		if(file_exists($path))
		{
			$contents = file_get_contents($path);

			$contents = preg_replace('/\<\!\-\- include (.*) \-\-\>/', '<?php $this->render("$1"); ?>', $contents);

			foreach ($this->vars as $key => $value)
			{
				if(!is_array($value))
				{
					$contents = preg_replace('/\{\{' . $key . '\}\}/', $value, $contents);
				}
			}

			$contents = preg_replace('/\{\{ (.*) \}\}/', '<?php echo $1; ?>', $contents);
			$contents = preg_replace('/\<\!\-\- if (.*) \-\-\>/', '<?php if($1) : ?>', $contents);
			$contents = preg_replace('/\<\!\-\- else \-\-\>/', '<?php else : ?>', $contents);
			$contents = preg_replace('/\<\!\-\- endif \-\-\>/', '<?php endif; ?>', $contents);


			$contents = preg_replace('/\<\!\-\- foreach (.*) \-\-\>/', '<?php foreach($1) { ?>', $contents);
			$contents = preg_replace('/\<\!\-\- endloop \-\-\>/', '<?php } ?>', $contents);


			$contents = preg_replace('/\<\!\-\- define (.*) \-\-\>/', '<?php $1; ?>', $contents);
			$contents = preg_replace('/\<\!\-\- print (.*) \-\-\>/', '<?php echo $1; ?>', $contents);
			$contents = preg_replace('/\<\!\-\- print_array (.*) (.*?) \-\-\>/', '<?php print_array($1,$2); ?>', $contents);

			eval(' ?>' . $contents . '<?php  ');
		}
		else
		{
			exit('<h1>Template Error</h1>');
		}
	}
}


?>