<?php

function getSubjectInformation($param)
{
    $param = getParameter($param,getLength($param)-1);
    $subject = new StdClass;
    $DB = DataBase::getInstance();
    $subject = $DB->get('subjects',array('subject_id', '=', $param));
    if($subject->count() == 1)
    {
        echo 'ID : ' . $subject->first()->id;
        echo '<br />Subject ID : ' . $subject->first()->subject_id;
        echo '<br />Subject Name : ' . $subject->first()->subject_name;
        echo '<br />Semester : ' . $subject->first()->subject_semester;
    }
    else
    {
        echo 'No Such Subject found.';
    }
}

function getAllSubjectsInformation()
{
    $subjects = new StdClass;
    $DB = DataBase::getInstance();
    $subjects = $DB->query('SELECT * FROM `subjects`');
    if($subjects->count())
    {
        foreach($subjects->results() as $subject)
        {
            echo 'ID : ' . $subject->id;
            echo '<br />Subject ID : <a href="subject/' . $subject->subject_id .'">' . $subject->subject_id .'</a>';
            echo '<br />Subject Name : ' . $subject->subject_name;
            echo '<br />Semester : ' . $subject->subject_semester;
            echo '<br /><br />';
        }
    }
    else
    {
        echo 'No Subjects found.';
    }
}
