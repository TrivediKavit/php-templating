<?php

function toArray($param)
{
    $param = trim($param,'/');
    $param = explode('/', $param);
    return $param;
}

function getLength($param)
{
    $param = toArray($param);
    return count($param);
}

function getParameter($param,$num)
{
    $param = toArray($param);
    return $param[$num];
}

function print_array($array,$separator=",")
{
    $count = 0;
    foreach ($array as $v)
    {
    	if($count != 0)
    	{
    		echo "{$separator}";
    	}
    	echo "{$v}";
    	$count++;
    }
}
